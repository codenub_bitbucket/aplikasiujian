import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, Platform, AlertController } from 'ionic-angular';


@Injectable()
export class GlobalProvider {

  lastBack    :     any;

  constructor(
    public http: HttpClient,
    public toastCtrl        : ToastController,
    public platform           : Platform,
    public alertCtrl        : AlertController,

  ) {
  }

  config(name:string)
  {
    var config = {
      "versionApp"      : "2.0.0",
      "build"           : "1",
      "apiUrlMode"      : 1, // 0=>offline, 1=>online || default 1=online
      "apiUrlOffline"   : "https://192.168.0.110/AdminPanel/api/v1/",
      "apiUrlOnline"    : "https://smkkartikatama.com/api/v1/",
      "requestTimeout"  : 60000, // 25second
    }

    return config[name];
  }

  toastDefault(msg:any="", duration:number=3000)
  {
    this.toastCtrl.create({
        message     : msg,
        duration    : duration,
    })
    .present();
  }

  closeApp()
  {
    let alertClose = this.alertCtrl.create({
      message: "Keluar dari aplikasi?",
      buttons: [
        {
          text: "Tidak",
        },
        {
          text: "Ya",
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });

    if(Date.now() - this.lastBack < 500) {
      this.platform.registerBackButtonAction(() => {
        alertClose.dismiss();
      });

      alertClose.onDidDismiss(() => {
        this.platform.registerBackButtonAction(() => {
          this.closeApp();
        });
      });

      alertClose.present();
    }

    this.lastBack = Date.now();
  }


}
