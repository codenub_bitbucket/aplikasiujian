import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter, Output } from '@angular/core';
import 'rxjs/add/operator/timeout';
import { GlobalProvider } from './global';

@Injectable()
export class RestApiProvider {

  apiUrl 			= this.global.config("apiUrlOnline");
  apiUrlWeb 	= this.global.config("apiUrlWeb");

  constructor(
    public http       : HttpClient,
    public global     : GlobalProvider,
  ) {
    var apiUrlMode 	= this.global.config("apiUrlMode");
		if(apiUrlMode == 0)
		{
			this.apiUrl = this.global.config("apiUrlOffline");
		}
  }

  login(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'siswa/login', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
        resolve(err);
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
  }

  daftar_pelajaran(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'mata-pelajaran/cari', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
        resolve(err);
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
  }

  soal_by_mata_pelajaran(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'soal/soal-by-mata-pelajaran', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
        resolve(err);
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
  }

  simpan_hasil(criteria:any)
  {
    return new Promise( (resolve, reject) =>
		{
			this.http.post(this.apiUrl + 'soal/simpan_hasil', JSON.stringify(criteria))
			.timeout(this.global.config("requestTimeout"))
			.subscribe(result => // response after API
			{
				resolve(result);
			}, (err) => // response before API
			{
        resolve(err);
				if (typeof err['name'] !== undefined && err['name'] == "HttpErrorResponse")
				{
					var resp1 = { status: 500, msg: "HttpErrorResponse", data: Array() };
				}
				else if (typeof err['name'] !== undefined && err['name'] == "TimeoutError")
				{
					var resp2 = { status: 500, msg: "TimeoutError", data: Array() };
					resolve(resp2);
				}
				else
				{
					resolve(err);
				}
			});
    });
  }

}
