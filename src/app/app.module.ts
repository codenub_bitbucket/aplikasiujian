import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RestApiProvider } from '../providers/rest-api';
import { GlobalProvider } from '../providers/global';
import { HttpClientModule } from '@angular/common/http';
import { MataPelajaranPage } from '../pages/mata-pelajaran/mata-pelajaran';
import { SoalPage } from './../pages/soal/soal';
import { SoalDetailPage } from '../pages/soal/soal_detail';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    MataPelajaranPage,
    SoalPage,
    SoalDetailPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MataPelajaranPage,
    SoalPage,
    SoalDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestApiProvider,
    GlobalProvider
  ]
})
export class AppModule {}
