import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform, ModalController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api';
import { MataPelajaranPage } from '../mata-pelajaran/mata-pelajaran';
import { SoalDetailPage } from './soal_detail';
import { GlobalProvider } from '../../providers/global';

@Component({
  selector: 'page-soal',
  templateUrl: 'soal.html',
})
export class SoalPage {

  id:number = 0;
  nama_mata_pelajaran: string = "";

  list_data:any = new Array();
  soal_sekarang:any = new Array()
  jawaban:any = [];
  isLast:boolean = false;

  loading : Loading;

  data_siswa :any;

  constructor(
    public navCtrl            : NavController,
    public navParams          : NavParams,
    public loadingCtrl        : LoadingController,
    public restApi            : RestApiProvider,
    public alertCtrl          : AlertController,
    public platform           : Platform,
    public modalCtrl          : ModalController,
    public global             : GlobalProvider,

  ) {
    this.id = this.navParams.get("id");
    this.nama_mata_pelajaran = this.navParams.get("nama_mata_pelajaran");

    this.data_siswa = JSON.parse(window.localStorage.getItem("data_siswa"));

    this.get_soal();
  }

  get_soal()
  {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...',
    });
    this.loading.present();
    this.restApi.soal_by_mata_pelajaran({id : this.id})
    .then(result =>
    {
      if (typeof result['status'] !== undefined && result['status'] == 200)
      {
        this.loading.dismiss();
        if (result['data'].length > 0)
        {
          result['data'].forEach((item, key) => {
            var final_jawaban = [];
            Object.keys(item['jawaban_soal']).forEach((i) => {
              final_jawaban.push({
                value : i, // a,b,c,d
                label : item['jawaban_soal'][i], // Jawaban A. Misal "Ini Ibu Budi"
              })
            });
            result['data'][key]['jawaban_soal'] = final_jawaban;
            result['data'][key]['nomor_soal'] = parseInt(item.nomor_soal);
          });
        }
        this.list_data = result['data'];
        this.mulai_soal();
      }
      else
      {
        this.loading.dismiss();
      }
    })
  }

  mulai_soal()
  {
    this.soal_sekarang = this.list_data[0];
    if (this.soal_sekarang.nomor_soal == this.list_data.length)
    {
      this.isLast = true;
    }
  }

  jawab(data)
  {
    this.jawaban = data;

    /* Set localstorage */
    let jawaban_tersimpan = JSON.parse(window.localStorage.getItem("jawaban_tersimpan"));

    if (jawaban_tersimpan !== undefined && jawaban_tersimpan !== null && jawaban_tersimpan.length > 0)
    {
      let find = jawaban_tersimpan.findIndex(r => r['nomor_soal'] == this.soal_sekarang.nomor_soal);

      if (find >= 0)
      {
        jawaban_tersimpan[find]['value'] = data.value;
        jawaban_tersimpan[find]['label'] = data.label;
      }
      else
      {
        data.nomor_soal = this.soal_sekarang.nomor_soal;
        jawaban_tersimpan.push(data);
      }
      window.localStorage.setItem("jawaban_tersimpan", JSON.stringify(jawaban_tersimpan));
    }
    else
    {
      data.nomor_soal = this.soal_sekarang.nomor_soal;
      let j = [];
      j.push(data);
      window.localStorage.setItem("jawaban_tersimpan", JSON.stringify(j));
    }
  }

  back()
  {
    let alert = this.alertCtrl.create({
      message: 'Jika anda keluar, jawaban dan pekerjaan anda tidak akan tersimpan!',
      subTitle: 'Keluar dari soal ?',
      buttons: [
        {
          text: "Tidak",
          role: 'cancel'
        },
        {
          text: "Ya",
          handler: () =>
          {
            this.loading = this.loadingCtrl.create({
              content: 'Mengeluarkan anda dari soal..',
            });
            this.loading.present();

            setTimeout(() => {
              this.navCtrl.setRoot(MataPelajaranPage);
              this.loading.dismiss();
            }, 2000);
          }
        },
      ]
    })
    alert.present();
  }

  sebelumnya()
  {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...',
      cssClass: 'my-loading-class'
    });
    this.loading.present();

    let currentKey = this.soal_sekarang.nomor_soal;
    let find = this.list_data.findIndex(r => r['nomor_soal'] == (currentKey - 1));
    if (find >= 0)
    {
      this.loading.dismiss();
      this.soal_sekarang = this.list_data[find];

      if (this.soal_sekarang.jawaban !== undefined)
      {
        this.jawaban = this.soal_sekarang.jawaban;
      }

      if (this.soal_sekarang.nomor_soal !== this.list_data.length)
      {
        this.isLast = false;
      }
    }
  }

  berikutnya(from="")
  {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...',
      cssClass: 'my-loading-class'
    });

    this.loading.present();

    let currentKey = this.soal_sekarang.nomor_soal;
    let find = this.list_data.findIndex(r => r['nomor_soal'] == currentKey);
    if (find >= 0)
    {
      this.loading.dismiss();
      /* Save answer first */
      this.list_data[find]['jawaban'] = this.jawaban;
      /* end save */
      this.jawaban = [];
      find = find + 1;
      console.log(this.list_data);


      if (this.list_data[find] !== undefined)
      {
        this.soal_sekarang = this.list_data[find];
        if (this.soal_sekarang.jawaban !== undefined)
        {
          this.jawaban = this.soal_sekarang.jawaban;
        }
      }

      if (this.soal_sekarang.nomor_soal == this.list_data.length)
      {
        this.isLast = true;
      }

      if (from == 'selesai')
      {
        this.selesai();
      }
    }
  }

  detail_soal(data:any)
  {
    data.nama_mata_pelajaran = this.nama_mata_pelajaran;
    this.modalCtrl.create(SoalDetailPage, { data : data }).present();
  }

  selesai()
  {
    let alert = this.alertCtrl.create({
      message: 'Tekan Ya jika anda ingin menyelesaikan soal.',
      subTitle: 'Selesaikan soal?',
      buttons: [
        {
          text: "Tidak",
          role: 'cancel'
        },
        {
          text: "Ya",
          handler: () =>
          {
            this.loading = this.loadingCtrl.create({
              content: 'Menyimpan hasil pekerjaan...',
            });
            this.loading.present();

            let final_data = {
              hasil               : this.list_data,
              siswa_id            : this.data_siswa.serial_id_siswa,
              kelas_id            : this.data_siswa.kelas_id_siswa,
              jurusan_id          : this.data_siswa.jurusan_id_siswa,
              mata_pelajaran_id   : this.id,
            }
            this.restApi.simpan_hasil(final_data)
            .then((result) =>
            {
              if (typeof result['status'] !== undefined && result['status'] == 200)
              {
                this.loading.dismiss();
                this.navCtrl.setRoot(MataPelajaranPage);
              }
              else
              {
                this.loading.dismiss();
                this.global.toastDefault(result['msg'])
              }
            });

          }
        },
      ]
    })
    alert.present();
  }

}
