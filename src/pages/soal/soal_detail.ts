import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform, ModalController, ViewController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api';
import { MataPelajaranPage } from '../mata-pelajaran/mata-pelajaran';

@Component({
  selector: 'page-soal',
  templateUrl: 'soal_detail.html',
})
export class SoalDetailPage {

  content:any = new Array();

  constructor(
    public navCtrl            : NavController,
    public navParams          : NavParams,
    public loadingCtrl        : LoadingController,
    public restApi            : RestApiProvider,
    public alertCtrl          : AlertController,
    public platform           : Platform,
    public modalCtrl          : ModalController,
    public viewCtrl           : ViewController,

  ) {
    this.content = this.navParams.get("data");
  }

  dismiss()
  {
    this.viewCtrl.dismiss();
  }



}
