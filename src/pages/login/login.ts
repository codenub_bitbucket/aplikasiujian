import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, Platform } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { RestApiProvider } from '../../providers/rest-api';
import { GlobalProvider } from '../../providers/global';
import { HomePage } from '../home/home';
import * as moment from 'moment';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  data  = {username: "", password: ""};
  passwordType = "password";
  passwordIcon = "visibility_off";

  errorLogin:boolean = false;
  errorLoginMsg:string = '';

  loginForm:FormGroup;
  loading:Loading;

  constructor(
    public navCtrl          : NavController,
    public navParams        : NavParams,
    public builder          : FormBuilder,
    public restApi          : RestApiProvider,
    public global           : GlobalProvider,
    public loadingCtrl      : LoadingController,
    public platform         : Platform
  ) {
    if (window.localStorage.getItem("isLogin") !== null && JSON.parse(window.localStorage.getItem("isLogin")) == "true")
    {
      this.navCtrl.setRoot(HomePage);
    }
    this.validateDefined();
  }

  validateDefined()
  {
    this.loginForm = this.builder.group({

      'username': [
        '',
        Validators.compose([Validators.required])
      ],
      'password': [
        '',
        Validators.compose([Validators.required])
      ],
    });
  }

  login()
  {
    this.loading = this.loadingCtrl.create({
      content: 'Silahkan tunggu...',
    });
    this.loading.present();
    this.restApi.login(this.data)
    .then(result =>
    {
      if (typeof result['status'] !== undefined && result['status'] == 200)
      {
        window.localStorage.setItem("data_siswa", JSON.stringify(result['data']));
        window.localStorage.setItem("isLogin", JSON.stringify("true"));
        window.localStorage.setItem("login_date", JSON.stringify(moment().format("DD-MMM-YYYY")));

        this.platform.registerBackButtonAction(() =>
        {
          this.global.toastDefault("Tombol diblokir.")
        });
        this.navCtrl.setRoot(HomePage);
        this.loading.dismiss();
      }
      else
      {
        this.loading.dismiss();
        this.global.toastDefault(result['msg']);
      }
    })
  }

  showHidePassword()
  {
    this.passwordType = this.passwordType == 'password' ? 'text' : 'password';
		this.passwordIcon = this.passwordIcon === 'visibility_off' ? 'visibility' : 'visibility_off';
  }

}
