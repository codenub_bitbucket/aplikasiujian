import { Component } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController, Platform } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { MataPelajaranPage } from '../mata-pelajaran/mata-pelajaran';
import { GlobalProvider } from '../../providers/global';
import * as moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data_siswa: any = {};
  loading:Loading;

  constructor(
    public navCtrl          : NavController,
    public loadingCtrl      : LoadingController,
    public alertCtrl        : AlertController,
    public platform           : Platform,
    public global             : GlobalProvider,

  ) {
    if (window.localStorage.getItem("isLogin") == null)
    {
      this.navCtrl.setRoot(LoginPage);
    }
    else
    {
      var login_date = JSON.parse(window.localStorage.getItem("login_date"));
      this.data_siswa = JSON.parse(window.localStorage.getItem("data_siswa"));

      if (login_date !== moment().format("DD-MMM-YYYY"))
      {
        window.localStorage.clear();
        this.navCtrl.setRoot(LoginPage);
      }
    }
  }

  logout()
  {
    let alert = this.alertCtrl.create({
      message: 'Anda yakin ingin keluar ?',
      buttons: [
        {
          text: "Tidak",
          role: 'cancel'
        },
        {
          text: "Ya",
          handler: () =>
          {
            this.loading = this.loadingCtrl.create({
              content: 'Logging you out..',
            });
            this.loading.present();

            setTimeout(() => {
              window.localStorage.clear();
              this.navCtrl.setRoot(LoginPage);
              this.loading.dismiss();
              this.platform.registerBackButtonAction(() =>
              {
                this.platform.exitApp();
              });
            }, 2000);
          }
        },
      ]
    })
    alert.present();
  }

  mulaiUjian()
  {
    this.platform.registerBackButtonAction(() =>
    {
      this.global.toastDefault("Tombol diblokir.")
    });
    this.navCtrl.setRoot(MataPelajaranPage);
  }

}
