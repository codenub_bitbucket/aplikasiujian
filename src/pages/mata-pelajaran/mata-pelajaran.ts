import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, Platform } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api';
import { HomePage } from '../home/home';
import { SoalPage } from '../soal/soal';

@Component({
  selector: 'page-mata-pelajaran',
  templateUrl: 'mata-pelajaran.html',
})
export class MataPelajaranPage {

  criteria:any = {
    'kelas_id'    : 0,
    'jurusan_id'  : 0,
    'siswa_id'    : 0
  }

  list_data:any = new Array();

  loading : Loading;

  constructor(
    public navCtrl            : NavController,
    public navParams          : NavParams,
    public loadingCtrl        : LoadingController,
    public restApi            : RestApiProvider,
    public platform           : Platform,

  ) {
    let data_siswa = JSON.parse(window.localStorage.getItem("data_siswa"));

    if (data_siswa !== null)
    {
      this.criteria.kelas_id    = data_siswa.kelas_id_siswa;
      this.criteria.jurusan_id  = data_siswa.jurusan_id_siswa;
      this.criteria.siswa_id  = data_siswa.serial_id_siswa;
      this.daftar_pelajaran();
    }
  }

  daftar_pelajaran(refresher:any = undefined)
  {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...',
    });
    this.loading.present();
    this.restApi.daftar_pelajaran(this.criteria)
    .then((result) =>
    {
      if (typeof result['status'] !== undefined && result['status'] == 200)
      {
        if (refresher !== undefined)
        {
          refresher.complete();
        }
        this.loading.dismiss();
        this.list_data = result['data'];
      }
      else
      {
        if (refresher !== undefined)
        {
          refresher.complete();
        }
        this.loading.dismiss();
      }
    })
  }

  home()
  {
    this.navCtrl.setRoot(HomePage);
  }

  mulai_mengerjakan(id, name)
  {
    this.navCtrl.setRoot(SoalPage, {id : id, nama_mata_pelajaran : name});
  }

  doRefresh(refresher)
  {
    this.daftar_pelajaran(refresher);
  }

}
